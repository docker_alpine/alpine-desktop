#!/bin/sh

. /usr/local/bin/docker-env

while [ -z "$(pgrep -f "Xvfb")" \
	-o -z "$(pgrep -f "xfdesktop")" \
	-o -z "$(pgrep -f "x11vnc")" ]
do
	sleep 1
done

sleep 3

if [ "${NOVNC_PORT}" != "-1" ]; then
	exec novnc_server --vnc localhost:${VNC_PORT:-5900} --listen ${NOVNC_PORT:-6080}
fi

