#!/bin/sh

. /usr/local/bin/docker-env

while [ -z "$(pgrep -f "Xvfb")" \
	-o -z "$(pgrep -f "xfdesktop")" ]
do
	sleep 1
done

sleep 3

if [ "${NO_XRDP:-N}" = "N" ]; then
	exec xrdp -n
fi

