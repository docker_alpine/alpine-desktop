#!/bin/sh

. /usr/local/bin/docker-env

if [ "${NO_SSHD:-N}" = "N" ]; then
	exec /usr/sbin/dropbear -r /conf.d/ssh/dropbear_ed25519_host_key -F -p 2222
fi

