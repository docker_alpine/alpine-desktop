#!/bin/sh

. /usr/local/bin/docker-env

while [ -z "$(pgrep -f "Xvfb")" \
	-o -z "$(pgrep -f "xfdesktop")" ]
do
	sleep 1
done

sleep 3 

USE_REPEAT=${USE_REPEAT:-N}
USE_X11VNC=${USE_X11VNC:-N}

if [ "${USE_X11VNC}" = "Y" ]; then
	exec su-exec ${RUN_USER_NAME} x11vnc --display ${DISPLAY} -xkb -noxrecord -noxfixes -noxdamage -shared -permitfiletransfer -tightfilexfer -usepw -loop -ncache 0 -scrollcopyrect always -wireframe always $([ "${USE_REPEAT}" = "Y" ] && echo '-repeat') -rfbport ${VNC_PORT:-5900} --rfbportv6 ${VNC_PORT:-5900}
else
	RUN_USER_HOME=$(eval echo ~${RUN_USER_NAME})
	exec su-exec ${RUN_USER_NAME} x0vncserver --display=${DISPLAY} --rfbport=${VNC_PORT:-5900} --PasswordFile=${RUN_USER_HOME}/.vnc/passwd
fi

