#!/bin/sh

. /usr/local/bin/docker-env

while [ -z "$(pgrep -f "Xvfb")" ]
do
	sleep 1
done

sleep 3

exec su-exec ${RUN_USER_NAME} dbus-launch /usr/bin/xfce4-session --display=${DISPLAY}

