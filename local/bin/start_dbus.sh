#!/bin/sh

. /usr/local/bin/docker-env

mkdir -p /var/run/dbus
chown -R messagebus:messagebus /var/run/dbus
exec su-exec messagebus dbus-daemon --system --nofork

