#!/bin/sh

. /usr/local/bin/docker-env

while [ -z "$(pgrep -f "Xvfb")" \
	-o -z "$(pgrep -f "xfwm4")" \
	-o -z "$(pgrep -f "xfdesktop")" ]
do
	sleep 1
done

sleep 3

if [ ! -z "$(which nabi 2> /dev/null)" ]; then
	DISPLAY=${DISPLAY} exec su-exec ${RUN_USER_NAME} nabi
elif [ ! -z "$(which uim-xim 2> /dev/null)" ]; then
	DISPLAY=${DISPLAY} su-exec ${RUN_USER_NAME} uim-xim
	DISPLAY=${DISPLAY} su-exec ${RUN_USER_NAME} uim-toolbar-gtk
elif [ ! -z "$(which ibus 2> /dev/null)" ]; then
	DISPLAY=${DISPLAY} su-exec ${RUN_USER_NAME} ibus-daemon -drx
fi

