#!/bin/sh

. /usr/local/bin/docker-env

while [ -z "$(pgrep -f "dbus-daemon --system")" ]
do
	sleep 1
done

exec su-exec ${RUN_USER_NAME} Xvfb ${DISPLAY} -screen 0 ${VNC_DESKTOP_GEOMETRY:-1024x768}x${VNC_DESKTOP_DEPTH:-16} -dpi 96 -ac +extension RANDR

