# alpine-desktop
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-desktop)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-desktop)



----------------------------------------
### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-desktop/x64)
### aarch64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-desktop/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : Desktop
    - xfce4 Desktop environment



----------------------------------------
#### Run

```sh
docker run -d -t \
              -e RUN_USER_NAME=<user_name> \
              -e RUN_USER_UID=<user_uid> \
              -e RUN_USER_GID=<user_gid> \
              -e VNC_PASSWD=<vnc_passwd> \
              -e VNC_DESKTOP_NAME=<vnc_desktop_name> \
              -e VNC_DESKTOP_GEOMETRY=<vnc_desktop_geometry> \
              -e VNC_DESKTOP_DEPTH=<vnc_desktop_depth> \
              forumi0721/alpine-desktop:[ARCH_TAG]
```



----------------------------------------
#### Usage

* URL : [vnc://localhost:5900](vnc://localhost:80)
    - Default user name : forumi0721/passwd



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -t                 | Allocate a pseudo-TTY                            |
| -cap-add=SYS_ADMIN | Add SYS_ADMIN Capability to run chromium         |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 5900/tcp           | VNC Port                                         |
| 3389/tcp           | RDP Port                                         |
| 2222/tcp           | SSH Port                                         |
| 6080/tcp           | noVNC Port                                       |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| RUN_USER_NAME      | Login user name (default:forumi0721)             |
| RUN_USER_UID       | Login user uid (default:1000)                    |
| RUN_USER_GID       | Login user gid (default:100)                     |
| VNC_PASSWD         | VNC password (default : passwd)                  |
| VNC_DESKTOP_NAME   | VNC desktop name (default : vnc)                 |
| VNC_DESKTOP_GEOMETRY| VNC desktop geometry (default : 1024x768)       |
| VNC_DESKTOP_DEPTH  | VNC desktop depth (default : 16)                 |
| VNC_PORT           | VNC Port (default : 5900)                        |
| NOVNC_PORT         | noVNC Port (default : 6080)                      |
| NO_XRDP            | xrdp use (default:N)                             |
| NO_SSHD            | sshd use (default:N)                             |
| USE_X11VNC         | Use X11 VNC (default:tigervnc)                   |

